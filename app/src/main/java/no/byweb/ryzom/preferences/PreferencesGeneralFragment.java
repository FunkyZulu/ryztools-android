package no.byweb.ryzom.preferences;

import java.io.File;

import no.byweb.ryzom.dev.R;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Environment;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.widget.Toast;

public class PreferencesGeneralFragment extends PreferenceFragment {
	static final String TAG = "PreferencesGeneralFragment";

	private final String DIRECTORY = Environment.getExternalStorageDirectory() + "/ryztools_cache/";

	Preference button;
	SharedPreferences prefs;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences_general);

		prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());

		button = (Preference) findPreference("btnResetPaths");
		button.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference arg0) {
				Editor editor = prefs.edit();
				editor.remove("api_base_path");
				editor.remove("key_manager_path");
				editor.apply();
				Toast.makeText(getActivity().getApplicationContext(), "Paths was reset.", Toast.LENGTH_LONG).show();
				getActivity().recreate();
				return true;
			}
		});

		button = (Preference) findPreference("btnClearCache");
		button.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference arg0) {
				File directory = new File(DIRECTORY);
				if (directory.exists()) {
					DeleteRecursive(directory);
				}
				Toast.makeText(getActivity().getApplicationContext(), "Icon cache was cleared.", Toast.LENGTH_LONG)
						.show();
				return true;
			}
		});
	}

	private static void DeleteRecursive(File fileOrDirectory) {
		if (fileOrDirectory.isDirectory()) {
			for (File child : fileOrDirectory.listFiles()) {
				DeleteRecursive(child);
			}
		}

		fileOrDirectory.delete();
	}

}
