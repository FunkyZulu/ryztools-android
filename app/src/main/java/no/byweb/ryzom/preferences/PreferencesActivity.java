package no.byweb.ryzom.preferences;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import no.byweb.ryzom.core.RyztoolsApp;
import no.byweb.ryzom.core.RyztoolsXmlDownloader;
import no.byweb.ryzom.dev.R;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class PreferencesActivity extends PreferenceActivity {
	static final String TAG = "PreferencesActivity";

	SharedPreferences prefs;
	RyztoolsXmlDownloader downloader;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
	}

	/**
	 * Populate the activity with the top-level headers.
	 */
	@Override
	public void onBuildHeaders(List<Header> target) {
		loadHeadersFromResource(R.xml.preferences_headers, target);
	}

	protected boolean isValidFragment(String fragmentName) {
		return true;
	}

	public void btnAddKey_onClick(View view) {
		AlertDialog.Builder builder = new AlertDialog.Builder(PreferencesActivity.this);

		builder.setTitle(R.string.dialog_add_key_title);

		LayoutInflater inflater = getLayoutInflater();
		builder.setView(inflater.inflate(R.layout.dialog_add_api_key, null));

		builder.setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				// User clicked OK button

				EditText text = (EditText) ((Dialog) dialog).findViewById(R.id.txtNewApiKey);

				addKey(text.getText().toString());
			}
		});
		builder.setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				// User cancelled the dialog

				dialog.dismiss();
			}
		});
		AlertDialog dialog = builder.create();
		dialog.show();
	}

	public void addKey(String key) {
		if (key.length() != 41) {
			Toast.makeText(getApplicationContext(), "Key not accepted.", Toast.LENGTH_LONG).show();
			return;
		}
		if ((!key.startsWith("c")) && (!key.startsWith("g"))) {
			Toast.makeText(getApplicationContext(), "Key not accepted.", Toast.LENGTH_LONG).show();
			return;
		}

		FileInputStream fis = null;
		boolean worked = true;
		try {
			fis = openFileInput(key + ".xml");
			byte[] buffer = new byte[fis.available()];
			while (fis.read(buffer) != -1) {
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			worked = false;
		} catch (IOException e) {
			e.printStackTrace();
			worked = false;
		} finally {
			try {
				fis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (!worked) {
			if (key.startsWith("c")) {
				downloader = new RyztoolsXmlDownloader(RyztoolsApp.URL_CHARACTER, key, this, true);
			} else {
				downloader = new RyztoolsXmlDownloader(RyztoolsApp.URL_GUILD, key, this, true);
			}
			downloader.execute();
		} else {
			Toast.makeText(getApplicationContext(), "Key already added.", Toast.LENGTH_LONG).show();
		}

	}

	public void btnGetKeys_onClick(View view) {
		String url = prefs.getString("key_manager_path", "http://app.ryzom.com/app_ryzomapi/");
		try {
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setData(Uri.parse(url));
			startActivity(intent);
		} catch (Exception e) {
			e.printStackTrace();
			Toast.makeText(getApplicationContext(), "Could not open page: " + url, Toast.LENGTH_SHORT).show();
		}
	}

}
