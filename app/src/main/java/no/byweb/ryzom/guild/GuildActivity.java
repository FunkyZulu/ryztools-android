package no.byweb.ryzom.guild;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.text.NumberFormat;

import no.byweb.ryzom.core.RyztoolsImageDownloader;
import no.byweb.ryzom.dev.R;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class GuildActivity extends Activity {
	static final String TAG = "GuildActivity";

	String XML_FILE;

	String guildName = null;
	String guildMotd = null;
	String guildIcon = null;
	String dappers = null;

	boolean worked = false;

	TextView textview;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_guild);

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			XML_FILE = extras.getString("XML_FILE");
			getData();
			if (worked) {
				if (guildName != null) {
					textview = (TextView) findViewById(R.id.txtGuildName);
					textview.setText(guildName);
				}

				if (guildMotd != null) {
					textview = (TextView) findViewById(R.id.txtGuildMotd);
					textview.setText(String.valueOf(guildMotd));
				}

				if (dappers != null) {
					textview = (TextView) findViewById(R.id.txtDappers);
					try {
						textview.setText("Dappers: " + NumberFormat.getInstance().format(Long.parseLong(dappers)));
					} catch (NumberFormatException e) {
						e.printStackTrace();
					}
				}

				if (guildIcon != null) {
					class ImageDownloader extends RyztoolsImageDownloader {

						public ImageDownloader(String url, String folderName, String fileName) {
							super(url, folderName, fileName);
						}

						@Override
						protected void onPostExecute(String result) {
							super.onPostExecute(result);

							if (getFile() == true) {
								final float scale = getResources().getDisplayMetrics().density;
								int pixels = (int) (64 * scale + 0.5f);

								ImageView iv = (ImageView) findViewById(R.id.imgGuildIcon);
								iv.getLayoutParams().width = pixels;
								iv.getLayoutParams().height = pixels;

								iv.setImageDrawable(Drawable.createFromPath(file.toString()));
							}
						}

					}
					ImageDownloader imageDownloader = new ImageDownloader("guild_icon.php?icon=" + guildIcon
							+ "&size=b", "guild", guildIcon);
					imageDownloader.execute();
				}

			}
		} else {
			finish();
		}
	}

	private void getData() {
		FileInputStream fis = null;
		String fileContent = null;
		XmlPullParserFactory factory;

		try {
			fis = getApplicationContext().openFileInput(XML_FILE);
			byte[] buffer = new byte[fis.available()];
			while (fis.read(buffer) != -1) {
				fileContent = new String(buffer);
			}
			factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XmlPullParser xpp = factory.newPullParser();
			xpp.setInput(new StringReader(fileContent));
			int eventType = xpp.getEventType();
			String tag = null;
			while (eventType != XmlPullParser.END_DOCUMENT) {
				String tagName = xpp.getName();
				switch (eventType) {
				case XmlPullParser.START_TAG:
					tag = tagName;
					break;
				case XmlPullParser.TEXT:
					if (xpp.getDepth() == 3) {
						try {
							if (tag.equals("name")) {
								guildName = xpp.getText();
							} else if (tag.equals("motd")) {
								guildMotd = xpp.getText();
							} else if (tag.equals("icon")) {
								guildIcon = xpp.getText();
							} else if (tag.equals("money")) {
								dappers = xpp.getText();
							}
						} catch (NumberFormatException e) {
							e.printStackTrace();
						}
					}
					break;
				default:
					break;
				}
				eventType = xpp.next();
			}
			worked = true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		}
	}

	public void btnInventory_onClick(View view) {
		Intent intent = new Intent(this, GuildInventoryActivity.class);
		intent.putExtra("XML_FILE", XML_FILE);
		startActivity(intent);
	}

	public void btnMembers_onClick(View view) {
		Intent intent = new Intent(this, GuildMembersActivity.class);
		intent.putExtra("XML_FILE", XML_FILE);
		startActivity(intent);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return false;
	}

}
