package no.byweb.ryzom.core;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.content.Context;
import android.util.Log;

public class FileHandler {
	static final String TAG = "FileHandler";

	public static String openInternalFile(String fileName, Context context) {
		FileInputStream fis = null;
		String fileContent = null;
		try {
			fis = context.openFileInput(fileName);
			byte[] buffer = new byte[fis.available()];
			if (fis.available() != 0) {
				while (fis.read(buffer) != -1) {
					fileContent = new String(buffer);
				}
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				fis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return fileContent;
	}

	public static boolean saveFileInternally(String fileName, byte[] bs, Context context) {
		boolean ret = false;
		FileOutputStream fos = null;
		try {
			fos = context.openFileOutput(fileName, Context.MODE_PRIVATE);
			fos.write(bs);
			ret = true;

			Log.d(TAG, "File (" + fileName + ") was saved.");
		} catch (IOException e) {
			e.printStackTrace();

			Log.d(TAG, "The file (" + fileName + ") could not be saved.");
		} finally {
			try {
				fos.close();
				return ret;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return ret;
	}

}
