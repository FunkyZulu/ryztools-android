package no.byweb.ryzom.core;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

public class RyztoolsApp extends Application {
	static final String TAG = "RyztoolsApp";

	SharedPreferences prefs;

	public static String apiBasePath;
	public static final String URL_TIME = "time.php?format=xml";
	public static final String URL_CHARACTER = "character.php?apikey=";
	public static final String URL_GUILD = "guild.php?apikey=";

	RyztoolsXmlDownloaderCore downloader;
	Timer updater;

	private static boolean activityVisible;

	@Override
	public void onCreate() {
		super.onCreate();
		prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

		apiBasePath = prefs.getString("api_base_path", "http://api.ryzom.com/");

		downloader = new RyztoolsXmlDownloaderCore(this);
	}

	@Override
	public void onTerminate() {
		super.onTerminate();
		updater.cancel();
		activityVisible = false;
	}

	public boolean isActivityVisible() {
		return activityVisible;
	}

	public void activityResumed() {
		activityVisible = true;

		TimerTask task = new TimerTask() {

			@Override
			public void run() {
				try {
					contentUpdater();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		};
		updater = new Timer();
		updater.schedule(task, 500, 15 * 1000);
	}

	public void activityPaused() {
		activityVisible = false;
		updater.cancel();
	}

	public JSONObject getSeasonInfo() {
		JSONObject json = new JSONObject();

		FileInputStream fis = null;
		String fileContent = null;
		try {
			fis = openFileInput("time.xml");
			byte[] buffer = new byte[fis.available()];
			while (fis.read(buffer) != -1) {
				fileContent = new String(buffer);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				fis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (fileContent != null) {
			XmlPullParserFactory factory;
			try {
				factory = XmlPullParserFactory.newInstance();
				factory.setNamespaceAware(true);
				XmlPullParser xpp = factory.newPullParser();
				xpp.setInput(new StringReader(fileContent));
				int eventType = xpp.getEventType();
				String tag = null;
				while (eventType != XmlPullParser.END_DOCUMENT) {
					String tagName = xpp.getName();
					switch (eventType) {
					case XmlPullParser.START_TAG:
						tag = tagName;
						if (tag.equals("cache")) {
							json.put("created", Long.parseLong(xpp.getAttributeValue(null, "created")));
						}
						break;
					case XmlPullParser.TEXT:
						if (tag.equals("server_tick")) {
							json.put("server_tick", Long.parseLong(xpp.getText()));
						}
						break;
					default:
						break;
					}
					eventType = xpp.next();
				}
				return json;
			} catch (XmlPullParserException e1) {
				e1.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return null;

	}

	/**
	 * The content updater will check if the content needs an update, and if so,
	 * request one.
	 */

	private void contentUpdater() {
		updateTime();
		updateCharacters();
		updateGuilds();
	}

	public boolean checkIfUpdateNeeded(String fileContent, String url, String filename) {

		boolean ret = false;
		long unixTime = System.currentTimeMillis() / 1000L;

		if (fileContent != null) {
			XmlPullParserFactory factory;
			try {
				factory = XmlPullParserFactory.newInstance();
				factory.setNamespaceAware(true);
				XmlPullParser xpp = factory.newPullParser();
				xpp.setInput(new StringReader(fileContent));
				int eventType = xpp.getEventType();
				String tag = null;
				while (eventType != XmlPullParser.END_DOCUMENT) {
					String tagName = xpp.getName();

					switch (eventType) {
					case XmlPullParser.START_TAG:
						tag = tagName;
						if ((url.equals(RyztoolsApp.URL_TIME)) && (tag.equals("cache"))) {
							if (xpp.getAttributeValue(null, "expire") != null) {
								// The expire time is only one minute.
								// To save some network traffic, we add a few more minutes.
								Long expire = Long.parseLong(xpp.getAttributeValue(null, "expire")) + (60 * 4);
								if (unixTime - expire < 0) {
									Log.d(TAG, filename + " is still valid for another " + -(unixTime - expire)
											+ " secs");
								} else {
									Log.d(TAG, filename + " expired " + (unixTime - expire) + " secs ago, getting new.");
									ret = true;
								}
							}
						} else if ((xpp.getDepth() == 2) && (url.equals(RyztoolsApp.URL_CHARACTER))
								&& (tag.equals("character"))) {
							if (xpp.getAttributeValue(null, "cached_until") != null) {
								Long expire = Long.parseLong(xpp.getAttributeValue(null, "cached_until"));
								if (unixTime - expire < 0) {
									Log.d(TAG, filename + " is still valid for another " + -(unixTime - expire)
											+ " secs");
								} else {
									Log.d(TAG, filename + " expired " + (unixTime - expire) + " secs ago, getting new.");
									ret = true;
								}
							}
						} else if ((xpp.getDepth() == 2) && (url.equals(RyztoolsApp.URL_GUILD))
								&& (tag.equals("guild"))) {
							if (xpp.getAttributeValue(null, "cached_until") != null) {
								Long expire = Long.parseLong(xpp.getAttributeValue(null, "cached_until"));
								if (unixTime - expire < 0) {
									Log.d(TAG, filename + " is still valid for another " + -(unixTime - expire)
											+ " secs");
								} else {
									Log.d(TAG, filename + " expired " + (unixTime - expire) + " secs ago, getting new.");
									ret = true;
								}
							}
						}
						break;
					default:
						break;
					}
					eventType = xpp.next();
				}
			} catch (XmlPullParserException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			Log.d(TAG, "File (" + filename + ") not found, fetching it.");
			ret = true;
		}
		return ret;
	}

	private void updateTime() {
		String fileContent = FileHandler.openInternalFile("time.xml", RyztoolsApp.this);

		if (fileContent != null) {
			if (checkIfUpdateNeeded(fileContent, URL_TIME, "time.xml")) {
				downloader.run(URL_TIME, null);
			}
		} else {
			downloader.run(URL_TIME, null);
		}
	}

	private void updateCharacters() {
		String[] SavedFiles = getApplicationContext().fileList();

		for (int i = 0; i < SavedFiles.length; i++) {
			if ((SavedFiles[i].length() == 45) && (SavedFiles[i].startsWith("c"))) {

				String fileContent = FileHandler.openInternalFile(SavedFiles[i], RyztoolsApp.this);

				if (fileContent != null) {
					if (checkIfUpdateNeeded(fileContent, URL_CHARACTER, SavedFiles[i])) {
						downloader.run(URL_CHARACTER, SavedFiles[i].substring(0, 41));
					}
				} else {
					downloader.run(URL_CHARACTER, SavedFiles[i].substring(0, 41));
				}
			}
		}
	}

	private void updateGuilds() {
		String[] SavedFiles = getApplicationContext().fileList();

		for (int i = 0; i < SavedFiles.length; i++) {
			if ((SavedFiles[i].length() == 45) && (SavedFiles[i].startsWith("g"))) {

				String fileContent = FileHandler.openInternalFile(SavedFiles[i], RyztoolsApp.this);

				if (fileContent != null) {
					if (checkIfUpdateNeeded(fileContent, URL_GUILD, SavedFiles[i])) {
						downloader.run(URL_GUILD, SavedFiles[i].substring(0, 41));
					}
				} else {
					downloader.run(URL_GUILD, SavedFiles[i].substring(0, 41));
				}
			}
		}
	}

	public static boolean validateXml(String xml, String url) {
		XmlPullParserFactory factory;
		try {
			factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XmlPullParser xpp = factory.newPullParser();
			xpp.setInput(new StringReader(xml));
			int eventType = xpp.getEventType();
			String tag = null;
			int checks;
			if (url.equals(RyztoolsApp.URL_TIME)) {
				checks = 3;
			} else if ((url.equals(RyztoolsApp.URL_CHARACTER)) || (url.equals(RyztoolsApp.URL_GUILD))) {
				checks = 3;
			} else {
				checks = 0;
			}
			while (eventType != XmlPullParser.END_DOCUMENT) {
				String tagName = xpp.getName();
				switch (eventType) {
				case XmlPullParser.START_TAG:
					tag = tagName;
					if (((url.equals(RyztoolsApp.URL_TIME)) && (tag.equals("cache")))
							|| ((xpp.getDepth() == 2) && (url.equals(RyztoolsApp.URL_CHARACTER)) && (tag
									.equals("character")))
							|| ((xpp.getDepth() == 2) && (url.equals(RyztoolsApp.URL_GUILD)) && (tag.equals("guild")))) {
						if (xpp.getAttributeValue(null, "created") != null) {
							try {
								Long.parseLong(xpp.getAttributeValue(null, "created"));
								checks--;
							} catch (Exception e) {
								Log.d(TAG, "Time created attribute could not be read.");
							}
						}
						if ((url.equals(RyztoolsApp.URL_TIME)) && (xpp.getAttributeValue(null, "expire") != null)) {
							try {
								Long.parseLong(xpp.getAttributeValue(null, "expire"));
								checks--;
							} catch (Exception e) {
								Log.d(TAG, "Time expire attribute could not be read.");
							}
						} else if (xpp.getAttributeValue(null, "cached_until") != null) {
							try {
								Long.parseLong(xpp.getAttributeValue(null, "cached_until"));
								checks--;
							} catch (Exception e) {
								Log.d(TAG, "Time expire attribute could not be read.");
							}
						}
					}
					break;
				case XmlPullParser.TEXT:
					if (url.equals(RyztoolsApp.URL_TIME)) {
						if (tag.equals("server_tick")) {
							try {
								Long.parseLong(xpp.getText());
								checks--;
							} catch (Exception e) {
								Log.d(TAG, "Time server_tick could not be read.");
							}
						}
					} else if ((url.equals(RyztoolsApp.URL_CHARACTER)) || (url.equals(RyztoolsApp.URL_GUILD))) {
						if ((xpp.getDepth() == 3) && (tag.equals("name")) && (xpp.getText().length() > 0)) {
							checks--;
						}
					}
					break;
				case XmlPullParser.END_TAG:
					tag = tagName;
					break;
				default:
					break;
				}
				eventType = xpp.next();
			}
			if (checks == 0) {
				return true;
			}
			Log.d(TAG, "Xml validation checks failed: " + checks);
			return false;
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

}
