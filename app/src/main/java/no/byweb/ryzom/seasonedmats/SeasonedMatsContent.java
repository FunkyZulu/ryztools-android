package no.byweb.ryzom.seasonedmats;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SeasonedMatsContent {

	public static List<SeasonedMatsItem> ITEMS = new ArrayList<SeasonedMatsItem>();

	public static Map<String, SeasonedMatsItem> ITEM_MAP = new HashMap<String, SeasonedMatsItem>();

	static {
		addItem(new SeasonedMatsItem("1", "Amber", "mp_amber.png"));
		addItem(new SeasonedMatsItem("2", "Bark", "mp_bark.png"));
		addItem(new SeasonedMatsItem("3", "Fiber", "mp_fiber.png"));
		addItem(new SeasonedMatsItem("4", "Oil", "mp_oil.png"));
		addItem(new SeasonedMatsItem("5", "Resin", "mp_resin.png"));
		addItem(new SeasonedMatsItem("6", "Sap", "mp_sap.png"));
		addItem(new SeasonedMatsItem("7", "Seed", "mp_seed.png"));
		addItem(new SeasonedMatsItem("8", "Shell", "mp_shell.png"));
		addItem(new SeasonedMatsItem("9", "Wood", "mp_wood.png"));
		addItem(new SeasonedMatsItem("10", "Node", "mp_wood_node.png"));
	}

	private static void addItem(SeasonedMatsItem item) {
		ITEMS.add(item);
		ITEM_MAP.put(item.id, item);
	}

	public static class SeasonedMatsItem {
		private String id;
		private String name;
		private String icon;

		public SeasonedMatsItem(String id, String name, String icon) {
			this.id = id;
			this.name = name;
			this.icon = icon;
		}

		public String getId() {
			return id;
		}

		public String getName() {
			return name;
		}

		public String getIcon() {
			return icon;
		}

	}
}
