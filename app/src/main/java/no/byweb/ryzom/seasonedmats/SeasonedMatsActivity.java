package no.byweb.ryzom.seasonedmats;

import no.byweb.ryzom.dev.R;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;

public class SeasonedMatsActivity extends FragmentActivity implements MaterialListFragment.Callbacks {

	private boolean mTwoPane;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_seasoned_mats_list);

		getActionBar().setDisplayHomeAsUpEnabled(true);

		if (findViewById(R.id.material_detail_container) != null) {
			mTwoPane = true;

			((MaterialListFragment) getSupportFragmentManager().findFragmentById(R.id.material_list))
					.setActivateOnItemClick(true);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onItemSelected(String id) {
		if (mTwoPane) {
			Bundle arguments = new Bundle();
			arguments.putString(MaterialDetailFragment.ARG_ITEM_ID, id);
			MaterialDetailFragment fragment = new MaterialDetailFragment();
			fragment.setArguments(arguments);
			getSupportFragmentManager().beginTransaction().replace(R.id.material_detail_container, fragment).commit();

		} else {
			Intent detailIntent = new Intent(this, SeasonedMatsDetailActivity.class);
			detailIntent.putExtra(MaterialDetailFragment.ARG_ITEM_ID, id);
			startActivity(detailIntent);
		}
	}
}
