package no.byweb.ryzom.seasonedmats;

import android.database.Cursor;
import android.database.SQLException;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;

import no.byweb.ryzom.core.DbRyztools;
import no.byweb.ryzom.dev.R;

public class MaterialDetailFragment extends Fragment {
	static final String TAG = "MaterialDetailFragment";

	public static final String ARG_ITEM_ID = "item_id";

	private SeasonedMatsContent.SeasonedMatsItem mItem;
	protected DbRyztools myDbHelper;

	public MaterialDetailFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		myDbHelper = new DbRyztools(getActivity().getApplicationContext());
		myDbHelper.openDataBase();

		if (getArguments().containsKey(ARG_ITEM_ID)) {
			// Load the dummy content specified by the fragment
			// arguments. In a real-world scenario, use a Loader
			// to load content from a content provider.
			mItem = SeasonedMatsContent.ITEM_MAP.get(getArguments().getString(ARG_ITEM_ID));
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_seasoned_mats_detail, container, false);

		// Show the dummy content as text in a TextView.
		if (mItem != null) {
			((TextView) rootView.findViewById(R.id.txtMaterialName)).setText(mItem.getName());

			if (mItem.getName().equals("Amber")) {
				((TextView) rootView.findViewById(R.id.txtSuperNodes)).setText("Supernodes: Summer");
			} else if (mItem.getName().equals("Bark")) {
				((TextView) rootView.findViewById(R.id.txtSuperNodes)).setText("Supernodes: Spring and summer");
			} else if (mItem.getName().equals("Fiber")) {
				((TextView) rootView.findViewById(R.id.txtSuperNodes)).setText("Supernodes: Autumn and winter");
			} else if (mItem.getName().equals("Oil")) {
				((TextView) rootView.findViewById(R.id.txtSuperNodes)).setText("Supernodes: Spring and autumn");
			} else if (mItem.getName().equals("Resin")) {
				((TextView) rootView.findViewById(R.id.txtSuperNodes)).setText("Supernodes: Summer and winter");
			} else if (mItem.getName().equals("Sap")) {
				((TextView) rootView.findViewById(R.id.txtSuperNodes)).setText("Supernodes: Spring and summer");
			} else if (mItem.getName().equals("Seed")) {
				((TextView) rootView.findViewById(R.id.txtSuperNodes)).setText("Supernodes: Autumn and winter");
			} else if (mItem.getName().equals("Shell")) {
				((TextView) rootView.findViewById(R.id.txtSuperNodes)).setText("Supernodes: Autumn");
			} else if (mItem.getName().equals("Wood")) {
				((TextView) rootView.findViewById(R.id.txtSuperNodes)).setText("Supernodes: Winter");
			} else if (mItem.getName().equals("Node")) {
				((TextView) rootView.findViewById(R.id.txtSuperNodes)).setText("Supernodes: Spring");
			}
			TextView txtNewField;
			TableRow tr;
			LayoutParams txtLp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
			Cursor cursor = null;

			TableLayout tblSurfaceExcellents = (TableLayout) rootView.findViewById(R.id.tblSurfaceExcellents);

			try {
				cursor = myDbHelper.getReadableDatabase().query("seasoned_mats",
						new String[] { "type", "spring", "summer", "autumn", "winter" },
						"family = '" + mItem.getName() + "' AND location = 'Surface'", null, null, null, "type ASC");
				if (cursor.moveToFirst()) {
					while (!cursor.isAfterLast()) {

						tr = new TableRow(getActivity());
						txtNewField = new TextView(getActivity());
						txtNewField.setLayoutParams(txtLp);
						txtNewField.setText(cursor.getString(0));
						tr.addView(txtNewField);
						txtNewField = new TextView(getActivity());
						txtNewField.setLayoutParams(txtLp);
						txtNewField.setText((cursor.getString(1) != null ? cursor.getString(1) : "-"));
						txtNewField.setGravity(Gravity.CENTER);
						tr.addView(txtNewField);
						txtNewField = new TextView(getActivity());
						txtNewField.setLayoutParams(txtLp);
						txtNewField.setText((cursor.getString(2) != null ? cursor.getString(2) : "-"));
						txtNewField.setGravity(Gravity.CENTER);
						tr.addView(txtNewField);
						txtNewField = new TextView(getActivity());
						txtNewField.setLayoutParams(txtLp);
						txtNewField.setText((cursor.getString(3) != null ? cursor.getString(3) : "-"));
						txtNewField.setGravity(Gravity.CENTER);
						tr.addView(txtNewField);
						txtNewField = new TextView(getActivity());
						txtNewField.setLayoutParams(txtLp);
						txtNewField.setText((cursor.getString(4) != null ? cursor.getString(4) : "-"));
						txtNewField.setGravity(Gravity.CENTER);
						tr.addView(txtNewField);
						tblSurfaceExcellents.addView(tr, new TableLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
								LayoutParams.WRAP_CONTENT));

						cursor.moveToNext();
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (cursor != null) {
					cursor.close();
				}
			}

			TableLayout tblUnderSpring = (TableLayout) rootView.findViewById(R.id.tblUnderSpring);

			try {
				cursor = myDbHelper.getReadableDatabase().query("seasoned_mats",
						new String[] { "type", "spring", "summer", "autumn", "winter" },
						"family = '" + mItem.getName() + "' AND location = 'US'", null, null, null, "type ASC");
				if (cursor.moveToFirst()) {
					while (!cursor.isAfterLast()) {

						tr = new TableRow(getActivity());
						txtNewField = new TextView(getActivity());
						txtNewField.setLayoutParams(txtLp);
						txtNewField.setText(cursor.getString(0));
						tr.addView(txtNewField);
						txtNewField = new TextView(getActivity());
						txtNewField.setLayoutParams(txtLp);
						txtNewField.setText((cursor.getString(1) != null ? cursor.getString(1) : "-"));
						txtNewField.setGravity(Gravity.CENTER);
						tr.addView(txtNewField);
						txtNewField = new TextView(getActivity());
						txtNewField.setLayoutParams(txtLp);
						txtNewField.setText((cursor.getString(2) != null ? cursor.getString(2) : "-"));
						txtNewField.setGravity(Gravity.CENTER);
						tr.addView(txtNewField);
						txtNewField = new TextView(getActivity());
						txtNewField.setLayoutParams(txtLp);
						txtNewField.setText((cursor.getString(3) != null ? cursor.getString(3) : "-"));
						txtNewField.setGravity(Gravity.CENTER);
						tr.addView(txtNewField);
						txtNewField = new TextView(getActivity());
						txtNewField.setLayoutParams(txtLp);
						txtNewField.setText((cursor.getString(4) != null ? cursor.getString(4) : "-"));
						txtNewField.setGravity(Gravity.CENTER);
						tr.addView(txtNewField);
						tblUnderSpring.addView(tr, new TableLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
								LayoutParams.WRAP_CONTENT));

						cursor.moveToNext();
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (cursor != null) {
					cursor.close();
				}
			}

			TableLayout tblLandOfContinuity = (TableLayout) rootView.findViewById(R.id.tblLandOfContinuity);

			try {
				cursor = myDbHelper.getReadableDatabase().query("seasoned_mats",
						new String[] { "type", "spring", "summer", "autumn", "winter" },
						"family = '" + mItem.getName() + "' AND location = 'LoC'", null, null, null, "type ASC");
				if (cursor.moveToFirst()) {
					while (!cursor.isAfterLast()) {

						tr = new TableRow(getActivity());
						txtNewField = new TextView(getActivity());
						txtNewField.setLayoutParams(txtLp);
						txtNewField.setText(cursor.getString(0));
						tr.addView(txtNewField);
						txtNewField = new TextView(getActivity());
						txtNewField.setLayoutParams(txtLp);
						txtNewField.setText((cursor.getString(1) != null ? cursor.getString(1) : "-"));
						txtNewField.setGravity(Gravity.CENTER);
						tr.addView(txtNewField);
						txtNewField = new TextView(getActivity());
						txtNewField.setLayoutParams(txtLp);
						txtNewField.setText((cursor.getString(2) != null ? cursor.getString(2) : "-"));
						txtNewField.setGravity(Gravity.CENTER);
						tr.addView(txtNewField);
						txtNewField = new TextView(getActivity());
						txtNewField.setLayoutParams(txtLp);
						txtNewField.setText((cursor.getString(3) != null ? cursor.getString(3) : "-"));
						txtNewField.setGravity(Gravity.CENTER);
						tr.addView(txtNewField);
						txtNewField = new TextView(getActivity());
						txtNewField.setLayoutParams(txtLp);
						txtNewField.setText((cursor.getString(4) != null ? cursor.getString(4) : "-"));
						txtNewField.setGravity(Gravity.CENTER);
						tr.addView(txtNewField);
						tblLandOfContinuity.addView(tr, new TableLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
								LayoutParams.WRAP_CONTENT));

						cursor.moveToNext();
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (cursor != null) {
					cursor.close();
				}
			}

			TableLayout tblSunkenCity = (TableLayout) rootView.findViewById(R.id.tblSunkenCity);

			try {
				cursor = myDbHelper.getReadableDatabase().query("seasoned_mats",
						new String[] { "type", "spring", "summer", "autumn", "winter" },
						"family = '" + mItem.getName() + "' AND location = 'SC'", null, null, null, "type ASC");
				if (cursor.moveToFirst()) {
					while (!cursor.isAfterLast()) {

						tr = new TableRow(getActivity());
						txtNewField = new TextView(getActivity());
						txtNewField.setLayoutParams(txtLp);
						txtNewField.setText(cursor.getString(0));
						tr.addView(txtNewField);
						txtNewField = new TextView(getActivity());
						txtNewField.setLayoutParams(txtLp);
						txtNewField.setText((cursor.getString(1) != null ? cursor.getString(1) : "-"));
						txtNewField.setGravity(Gravity.CENTER);
						tr.addView(txtNewField);
						txtNewField = new TextView(getActivity());
						txtNewField.setLayoutParams(txtLp);
						txtNewField.setText((cursor.getString(2) != null ? cursor.getString(2) : "-"));
						txtNewField.setGravity(Gravity.CENTER);
						tr.addView(txtNewField);
						txtNewField = new TextView(getActivity());
						txtNewField.setLayoutParams(txtLp);
						txtNewField.setText((cursor.getString(3) != null ? cursor.getString(3) : "-"));
						txtNewField.setGravity(Gravity.CENTER);
						tr.addView(txtNewField);
						txtNewField = new TextView(getActivity());
						txtNewField.setLayoutParams(txtLp);
						txtNewField.setText((cursor.getString(4) != null ? cursor.getString(4) : "-"));
						txtNewField.setGravity(Gravity.CENTER);
						tr.addView(txtNewField);
						tblSunkenCity.addView(tr, new TableLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
								LayoutParams.WRAP_CONTENT));

						cursor.moveToNext();
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (cursor != null) {
					cursor.close();
				}
			}

			TableLayout tblForbiddenDepths = (TableLayout) rootView.findViewById(R.id.tblForbiddenDepths);

			try {
				cursor = myDbHelper.getReadableDatabase().query("seasoned_mats",
						new String[] { "type", "spring", "summer", "autumn", "winter" },
						"family = '" + mItem.getName() + "' AND location = 'FD'", null, null, null, "type ASC");
				if (cursor.moveToFirst()) {
					while (!cursor.isAfterLast()) {

						tr = new TableRow(getActivity());
						txtNewField = new TextView(getActivity());
						txtNewField.setLayoutParams(txtLp);
						txtNewField.setText(cursor.getString(0));
						tr.addView(txtNewField);
						txtNewField = new TextView(getActivity());
						txtNewField.setLayoutParams(txtLp);
						txtNewField.setText((cursor.getString(1) != null ? cursor.getString(1) : "-"));
						txtNewField.setGravity(Gravity.CENTER);
						tr.addView(txtNewField);
						txtNewField = new TextView(getActivity());
						txtNewField.setLayoutParams(txtLp);
						txtNewField.setText((cursor.getString(2) != null ? cursor.getString(2) : "-"));
						txtNewField.setGravity(Gravity.CENTER);
						tr.addView(txtNewField);
						txtNewField = new TextView(getActivity());
						txtNewField.setLayoutParams(txtLp);
						txtNewField.setText((cursor.getString(3) != null ? cursor.getString(3) : "-"));
						txtNewField.setGravity(Gravity.CENTER);
						tr.addView(txtNewField);
						txtNewField = new TextView(getActivity());
						txtNewField.setLayoutParams(txtLp);
						txtNewField.setText((cursor.getString(4) != null ? cursor.getString(4) : "-"));
						txtNewField.setGravity(Gravity.CENTER);
						tr.addView(txtNewField);
						tblForbiddenDepths.addView(tr, new TableLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
								LayoutParams.WRAP_CONTENT));

						cursor.moveToNext();
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (cursor != null) {
					cursor.close();
				}
			}

			myDbHelper.close();
		}

		return rootView;
	}
}
