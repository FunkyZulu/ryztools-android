package no.byweb.ryzom.character;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import no.byweb.ryzom.core.DbRyzomExtra;
import no.byweb.ryzom.dev.R;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.TabHost.TabSpec;

public class CharacterSkillsActivity extends Activity {
	static final String TAG = "CharacterSkillsActivity";

	String XML_FILE;
	private List<Skill> allSkills = new ArrayList<Skill>();
	private List<Skill> fightSkills = new ArrayList<Skill>();
	private List<Skill> magicSkills = new ArrayList<Skill>();
	private List<Skill> craftSkills = new ArrayList<Skill>();
	private List<Skill> forageSkills = new ArrayList<Skill>();

	private List<String> animated = new ArrayList<String>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_character_skills);

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			XML_FILE = extras.getString("XML_FILE");

			TabHost tabHost = (TabHost) findViewById(R.id.tabhost);
			tabHost.setup();

			TabSpec tabSpec = tabHost.newTabSpec("tag1");
			tabSpec.setContent(R.id.tab1);
			tabSpec.setIndicator("Fight");
			tabHost.addTab(tabSpec);

			tabSpec = tabHost.newTabSpec("tag2");
			tabSpec.setContent(R.id.tab2);
			tabSpec.setIndicator("Magic");
			tabHost.addTab(tabSpec);

			tabSpec = tabHost.newTabSpec("tag3");
			tabSpec.setContent(R.id.tab3);
			tabSpec.setIndicator("Crafting");
			tabHost.addTab(tabSpec);

			tabSpec = tabHost.newTabSpec("tag4");
			tabSpec.setContent(R.id.tab4);
			tabSpec.setIndicator("Foraging");
			tabHost.addTab(tabSpec);

			GetSkills skills = new GetSkills();
			skills.execute();
		} else {
			finish();
		}
	}

	public class GetSkills extends AsyncTask<String, Integer, String> {
		private XmlPullParserFactory factory;
		private FileInputStream fis = null;
		private String fileContent = null;
		DbRyzomExtra myDbHelper;

		private boolean inSkills = false;
		private String lastSkill;
		private String lastValue;
		private String name;
		private String location;

		@Override
		protected String doInBackground(String... arg0) {
			myDbHelper = new DbRyzomExtra(CharacterSkillsActivity.this);
			myDbHelper.openDataBase();

			try {
				fis = openFileInput(XML_FILE);
				byte[] buffer = new byte[fis.available()];
				while (fis.read(buffer) != -1) {
					fileContent = new String(buffer);
				}
				factory = XmlPullParserFactory.newInstance();
				factory.setNamespaceAware(true);
				XmlPullParser xpp = factory.newPullParser();
				xpp.setInput(new StringReader(fileContent));
				int eventType = xpp.getEventType();
				String tag = null;

				while (eventType != XmlPullParser.END_DOCUMENT) {
					String tagName = xpp.getName();
					switch (eventType) {
					case XmlPullParser.START_TAG:
						tag = tagName;
						if (xpp.getDepth() == 3) {
							if (tag.equals("skills")) {
								inSkills = true;
							}
						}
						break;
					case XmlPullParser.TEXT:
						if ((inSkills == true) && (xpp.getDepth() == 4)) {
							if (lastSkill != null) {
								printSkill(tag);
							}
							if (tag.length() == 2) {
								location = tag;
							}

							lastSkill = tag;
							lastValue = xpp.getText();
						}
						break;
					case XmlPullParser.END_TAG:
						tag = tagName;
						if (xpp.getDepth() == 3) {
							inSkills = false;
						}
						break;
					default:
						break;
					}
					eventType = xpp.next();
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (XmlPullParserException e) {
				e.printStackTrace();
			}

			printSkill("");

			myDbHelper.close();

			return null;
		}

		private void printSkill(String thisSkill) {
			if ((lastSkill == null) || (thisSkill.length() == lastSkill.length() + 1)) {
				return;
			}

			name = lastSkill;
			Cursor cursor = null;
			try {
				cursor = myDbHelper.getReadableDatabase().query("words_en_skill", new String[] { "name" },
						"_id = '" + lastSkill + "'", null, null, null, null);
				if (cursor.moveToFirst()) {
					if (cursor.getString(0) != null) {
						name = cursor.getString(0);
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (cursor != null) {
					cursor.close();
				}
			}

			allSkills.add(new Skill(name, lastValue, location));
		}

		@Override
		protected void onPostExecute(String result) {
			Skill currentSkill;
			for (int i = 0; i < allSkills.size(); i++) {
				currentSkill = allSkills.get(i);
				if (currentSkill.getLocation().equals("sf")) {
					fightSkills.add(currentSkill);
				} else if (currentSkill.getLocation().equals("sm")) {
					magicSkills.add(currentSkill);
				} else if (currentSkill.getLocation().equals("sc")) {
					craftSkills.add(currentSkill);
				} else if (currentSkill.getLocation().equals("sh")) {
					forageSkills.add(currentSkill);
				}
			}

			populateListView(R.id.fightSkills, fightSkills, R.id.fightSkillsEmpty);
			populateListView(R.id.magicSkills, magicSkills, R.id.magicSkillsEmpty);
			populateListView(R.id.craftSkills, craftSkills, R.id.craftSkillsEmpty);
			populateListView(R.id.forageSkills, forageSkills, R.id.forageSkillsEmpty);
		}
	}

	private void populateListView(int id, List<Skill> items, int emptyId) {
		ArrayAdapter<Skill> adapter = null;
		try {
			adapter = new MyListAdapter(this, items);

			adapter.notifyDataSetChanged();
			ListView list = (ListView) findViewById(id);
			list.setEmptyView(findViewById(emptyId));

			list.setAdapter(adapter);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private class MyListAdapter extends ArrayAdapter<Skill> {

		private List<Skill> items;

		public MyListAdapter(Context context, List<Skill> items) {
			super(context, R.layout.list_item_preferences_api_key, items);
			this.items = items;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// Make sure we have a view to work with (may have been given a null)
			View itemView = convertView;
			if (itemView == null) {
				itemView = getLayoutInflater().inflate(R.layout.list_item_skills, parent, false);
			}

			Skill currentKey = items.get(position);

			TextView nameText = (TextView) itemView.findViewById(R.id.item_textName);
			ProgressBar progressBar = (ProgressBar) itemView.findViewById(R.id.item_percent);
			progressBar.setMax(25000);

			String message = "";
			if (currentKey.getName() != null) {
				message += currentKey.getName() + ": ";
			} else {
				message += "Unknown: ";
			}
			if (currentKey.getValue() != null) {
				if (animated.contains(message)) {
					progressBar.setProgress((int) (Float.parseFloat(currentKey.getValue()) * 100));
				} else {
					ProgressBarAnimation anim = new ProgressBarAnimation(progressBar, 1,
							(int) (Float.parseFloat(currentKey.getValue()) * 100));
					anim.setDuration(500);
					progressBar.startAnimation(anim);
					animated.add(message);
				}
				message += currentKey.getValue();
			} else {
				message += "Unknown";
				progressBar.setProgress(0);
			}

			nameText.setText(message);

			return itemView;
		}

	}

	public class ProgressBarAnimation extends Animation {
		private ProgressBar progressBar;
		private float from;
		private float to;

		public ProgressBarAnimation(ProgressBar progressBar, float from, float to) {
			super();
			this.progressBar = progressBar;
			this.from = from;
			this.to = to;
		}

		@Override
		protected void applyTransformation(float interpolatedTime, Transformation t) {
			super.applyTransformation(interpolatedTime, t);
			float value = from + (to - from) * interpolatedTime;
			progressBar.setProgress((int) value);
		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return false;
	}

	class Skill {
		private String name;
		private String value;
		private String location;

		public Skill(String name, String value, String location) {
			super();
			this.name = name;
			this.value = value;
			this.location = location;
		}

		public String getName() {
			return name;
		}

		public String getValue() {
			return value;
		}

		public String getLocation() {
			return location;
		}

	}

}
