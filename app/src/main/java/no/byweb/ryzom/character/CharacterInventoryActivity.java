package no.byweb.ryzom.character;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import no.byweb.ryzom.core.GetItems;
import no.byweb.ryzom.core.Item;
import no.byweb.ryzom.core.RyztoolsImageDownloader;
import no.byweb.ryzom.dev.R;
import android.os.Bundle;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.v4.app.NavUtils;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

public class CharacterInventoryActivity extends Activity implements OnClickListener {
	static final String TAG = "CharacterInventoryActivity";

	private List<Item> bagItems = new ArrayList<Item>();
	private List<Item> roomItems = new ArrayList<Item>();
	private List<Item> pet1Items = new ArrayList<Item>();
	private List<Item> pet2Items = new ArrayList<Item>();
	private List<Item> pet3Items = new ArrayList<Item>();
	private List<Item> pet4Items = new ArrayList<Item>();

	String XML_FILE;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_character_inventory);

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			XML_FILE = extras.getString("XML_FILE");

			TabHost tabHost = (TabHost) findViewById(R.id.tabhost);
			tabHost.setup();

			TabSpec tabSpec = tabHost.newTabSpec("tag1");
			tabSpec.setContent(R.id.tab1);
			tabSpec.setIndicator("Bag");
			tabHost.addTab(tabSpec);

			tabSpec = tabHost.newTabSpec("tag2");
			tabSpec.setContent(R.id.tab2);
			tabSpec.setIndicator("Room");
			tabHost.addTab(tabSpec);

			tabSpec = tabHost.newTabSpec("tag3");
			tabSpec.setContent(R.id.tab3);
			tabSpec.setIndicator("Pet 1");
			tabHost.addTab(tabSpec);

			tabSpec = tabHost.newTabSpec("tag4");
			tabSpec.setContent(R.id.tab4);
			tabSpec.setIndicator("Pet 2");
			tabHost.addTab(tabSpec);

			tabSpec = tabHost.newTabSpec("tag5");
			tabSpec.setContent(R.id.tab5);
			tabSpec.setIndicator("Pet 3");
			tabHost.addTab(tabSpec);

			tabSpec = tabHost.newTabSpec("tag6");
			tabSpec.setContent(R.id.tab6);
			tabSpec.setIndicator("Pet 4");
			tabHost.addTab(tabSpec);

			PopulateList updater = new PopulateList(null, this);
			updater.execute();
		} else {
			finish();
		}
	}

	private class PopulateList extends GetItems {

		public PopulateList(String filter, Context context) {
			super(XML_FILE, context);
		}

		@Override
		protected void onPostExecute(String result) {
			for (int i = 0; i < allItems.size(); i++) {
				Item item = allItems.get(i);
				switch (item.getLocationPlace()) {
				case GetItems.LOC_INVENTORY:
					bagItems.add(item);
					break;
				case GetItems.LOC_ROOM:
					roomItems.add(item);
					break;
				case GetItems.LOC_PET1:
					pet1Items.add(item);
					break;
				case GetItems.LOC_PET2:
					pet2Items.add(item);
					break;
				case GetItems.LOC_PET3:
					pet3Items.add(item);
					break;
				case GetItems.LOC_PET4:
					pet4Items.add(item);
					break;
				default:
					break;
				}
			}
			populateListView(R.id.bagList, bagItems, R.id.bagListEmpty);
			populateListView(R.id.roomList, roomItems, R.id.roomListEmpty);
			populateListView(R.id.pet1List, pet1Items, R.id.pet1ListEmpty);
			populateListView(R.id.pet2List, pet2Items, R.id.pet2ListEmpty);
			populateListView(R.id.pet3List, pet3Items, R.id.pet3ListEmpty);
			populateListView(R.id.pet4List, pet4Items, R.id.pet4ListEmpty);
		}
	}

	private void populateListView(int id, List<Item> items, int emptyId) {
		ArrayAdapter<Item> adapter = null;
		try {
			adapter = new MyListAdapter(this, items);

			adapter.notifyDataSetChanged();
			ListView list = (ListView) findViewById(id);
			list.setEmptyView(findViewById(emptyId));

			list.setAdapter(adapter);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private class MyListAdapter extends ArrayAdapter<Item> {

		private List<Item> items;

		public MyListAdapter(Context context, List<Item> items) {
			super(context, R.layout.list_item_item, items);
			this.items = items;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// Make sure we have a view to work with (may have been given a null)
			View itemView = convertView;
			if (itemView == null) {
				itemView = getLayoutInflater().inflate(R.layout.list_item_item, parent, false);
			}

			Collections.sort(items, new CustomComparator());
			Item currentKey = items.get(position);

			itemView.setTag(currentKey.getId());

			TextView nameText = (TextView) itemView.findViewById(R.id.item_textName);

			String message = "";
			if (currentKey.getName() != null) {
				message += currentKey.getName();
			}
			if ((currentKey.getHp() != null) && (!currentKey.getHp().equals("0"))) {
				message += " (HP: " + currentKey.getHp() + ")";
			}
			nameText.setText(message);
			itemView.setOnClickListener(CharacterInventoryActivity.this);
			itemView.setTag(currentKey);

			String itemIcon;
			itemIcon = currentKey.getSheet();
			if (currentKey.getColor() != null) {
				itemIcon += "&c=" + currentKey.getColor();
			}

			ImageDownloader imageDownloader = new ImageDownloader("item_icon.php?sheetid=" + itemIcon, "item",
					itemIcon.replace("&", "_"), (ImageView) itemView.findViewById(R.id.item_imgIcon), currentKey);
			imageDownloader.execute();

			return itemView;
		}

	}

	private class ImageDownloader extends RyztoolsImageDownloader {

		private ImageView iv;
		private Item item;

		public ImageDownloader(String url, String folderName, String fileName, ImageView iv, Item item) {
			super(url, folderName, fileName);
			this.iv = iv;
			this.item = item;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			final float scale = getResources().getDisplayMetrics().density;
			int font = (int) (10 * scale + 0.5f);
			int marg = (int) (1 * scale + 0.5f);
			int image = (int) (40 * scale + 0.5f);

			Bitmap bitmap;

			if (getFile() == true) {

				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inPreferredConfig = Bitmap.Config.ARGB_8888;
				Bitmap b = BitmapFactory.decodeFile(file.toString(), options);
				bitmap = Bitmap.createScaledBitmap(b, image, image, false);
			} else {
				bitmap = BitmapFactory.decodeResource(getResources(), R.raw.icon_unknown);
			}

			Bitmap bmOverlay = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_4444);
			Canvas canvas = new Canvas(bmOverlay);

			Paint paint = new Paint();
			paint.setColor(Color.WHITE);
			paint.setTypeface(Typeface.createFromAsset(getApplication().getAssets(), "fonts/ingame.ttf"));
			paint.setTextSize(font);

			Paint shadow = new Paint();
			shadow.setColor(Color.BLACK);
			shadow.setTypeface(Typeface.createFromAsset(getApplication().getAssets(), "fonts/ingame.ttf"));
			shadow.setTextSize(font);

			int width = bitmap.getWidth();
			int height = bitmap.getHeight();

			canvas.drawBitmap(bitmap, 0, 0, null);

			canvas.drawText("x" + item.getStack(), (marg * 2), height - marg, shadow);
			canvas.drawText("x" + item.getStack(), marg, height - (marg * 2), paint);

			if ((item.getQuality() != null) && (!item.getQuality().equals("0")) && (!item.getQuality().equals("1"))) {
				int stringLen = (int) paint.measureText(item.getQuality());
				canvas.drawText(item.getQuality(), (width - stringLen) - marg * 2, height - marg, shadow);
				canvas.drawText(item.getQuality(), (width - stringLen) - marg, height - (marg * 2), paint);
			}

			iv.setImageBitmap(bmOverlay);
		}

	}
	
	private class ImageDownloader2 extends RyztoolsImageDownloader {

		private ImageView iv;

		public ImageDownloader2(String url, String folderName, String fileName, ImageView iv) {
			super(url, folderName, fileName);
			this.iv = iv;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			final float scale = getResources().getDisplayMetrics().density;
			int image = (int) (40 * scale + 0.5f);

			Bitmap bitmap;

			if (getFile() == true) {
				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inPreferredConfig = Bitmap.Config.ARGB_8888;
				Bitmap b = BitmapFactory.decodeFile(file.toString(), options);
				bitmap = Bitmap.createScaledBitmap(b, image, image, false);
			} else {
				bitmap = BitmapFactory.decodeResource(getResources(), R.raw.icon_unknown);
			}

			Bitmap bmOverlay = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_4444);
			Canvas canvas = new Canvas(bmOverlay);
			canvas.drawBitmap(bitmap, 0, 0, null);

			iv.setImageBitmap(bmOverlay);
		}

	}

	private class CustomComparator implements Comparator<Item> {
		@Override
		public int compare(Item o1, Item o2) {
			return o1.getSort().compareTo(o2.getSort());
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return false;
	}

	@Override
	public void onClick(View view) {
		Item itemObj = (Item) view.getTag();

		Dialog dialog = new Dialog(this);
		dialog.setContentView(R.layout.dialog_locate_items);
		dialog.setTitle("Item details");

		dialog.show();
		
		TextView tv1 = (TextView) dialog.findViewById(R.id.txtItemName);
		TextView tv2 = (TextView) dialog.findViewById(R.id.txtItemLocations);
		
		tv1.setText(itemObj.getName());
		String text = "<p>";
		
		if (itemObj.getColorName() != null) {
			text += "Color: " + itemObj.getColorName() + "<br/>";
		}
		text += "HP: " + itemObj.getHp() + "<br/>";
		text += "Quality: " + itemObj.getQuality() + "<br/>";
		text += "Stack: " + itemObj.getStack() + "<br/>";
		if (itemObj.getHpbuff() != null) {
			text += "HP: " + itemObj.getHpbuff() + "<br/>";
		}
		if (itemObj.getFocusbuff() != null) {
			text += "Focus: " + itemObj.getFocusbuff() + "<br/>";
		}
		if (itemObj.getSapbuff() != null) {
			text += "Sap: " + itemObj.getSapbuff() + "<br/>";
		}
		if (itemObj.getStabuff() != null) {
			text += "Stamina: " + itemObj.getStabuff() + "<br/>";
		}
		text += "</p>";

		tv2.setText(Html.fromHtml(text));
		
		String itemIcon = itemObj.getSheet();
		if (itemObj.getColor() != null) {
			itemIcon += "&c=" + itemObj.getColor();
		}
		
		ImageDownloader2 imageDownloader = new ImageDownloader2("item_icon.php?sheetid=" + itemIcon, "item",
				itemIcon.replace("&", "_"), (ImageView) dialog.findViewById(R.id.imgItemIcon));
		imageDownloader.execute();
	}

}
