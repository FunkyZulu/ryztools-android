package no.byweb.ryzom.pages;

import no.byweb.ryzom.core.RyztoolsApp;
import no.byweb.ryzom.dev.R;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.app.Activity;

public class AboutActivity extends Activity {
	static final String TAG = "AboutActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return false;
	}

	@Override
	protected void onResume() {
		super.onResume();
		((RyztoolsApp) getApplication()).activityResumed();
	}

	@Override
	protected void onPause() {
		super.onPause();
		((RyztoolsApp) getApplication()).activityPaused();
	}

}
