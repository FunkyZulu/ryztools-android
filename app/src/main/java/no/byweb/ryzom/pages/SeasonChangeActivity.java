package no.byweb.ryzom.pages;

import java.sql.Date;
import java.util.Locale;

import no.byweb.ryzom.core.RyztoolsApp;
import no.byweb.ryzom.dev.R;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.text.Html;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.MenuItem;
import android.view.Window;
import android.widget.TextView;
import android.app.Activity;

public class SeasonChangeActivity extends Activity {
	static final String TAG = "SeasonChangeActivity";

	boolean running = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.activity_season_change);

		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return false;
	}

	@Override
	protected void onResume() {
		super.onResume();
		((RyztoolsApp) getApplication()).activityResumed();

		running = true;
		new Thread() {

			@Override
			public void run() {
				while (running) {
					Updater updater = new Updater();
					updater.execute();

					try {
						Thread.sleep(1 * 1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}.start();
	}

	@Override
	protected void onPause() {
		super.onPause();
		((RyztoolsApp) getApplication()).activityPaused();
		running = false;
	}

	class Updater extends AsyncTask<String, Integer, String> {

		/*
		 * Time stuff
		 */
		public static final int HOUR_TICKS = 1800;
		public static final int DAY_HOURS = 24;
		public static final int SEASON_DAYS = 90;
		public static final int MONTH_DAYS = 30;
		public static final int CYCLE_MONTHS = 12;
		public static final int JY_CYCLES = 4;
		public static final int WEEK_DAYS = 6;
		/* 0 = spring, 1 = summer, 2 = autumn, 3 = winter */
		public static final int CYCLE_SEASON = 4;
		/*
		 * Tick is offset on server of 61 days. My guess is that spring does not
		 * start the first day of a year.
		 */
		public static final int SEASON_OFFSET_TICKS = 61 * DAY_HOURS * HOUR_TICKS;

		public static final int START_JY = 2568;

		/*
		 * Seems like the tick runs about five - eight minutes slow during a
		 * season. So we need to add five minutes. (Better to be early than
		 * late).
		 */
		public static final int SEASON_MINUTES_OFFSET = 5;

		/* Helpers */
		public static final int CYCLE_DAYS = CYCLE_MONTHS * MONTH_DAYS;
		public static final int JY_DAYS = CYCLE_DAYS * JY_CYCLES;
		public static final int JY_MONTHS = CYCLE_MONTHS * JY_CYCLES;
		public static final int SEASON_TICKS = HOUR_TICKS * DAY_HOURS * SEASON_DAYS;
		public static final int JY_TICKS = SEASON_TICKS * JY_CYCLES;
		/*
		 * Time stuff done.
		 */

		JSONObject seasonInfo = null;

		long serverTick;
		long created;

		@Override
		protected String doInBackground(String... params) {
			seasonInfo = ((RyztoolsApp) getApplication()).getSeasonInfo();
			if (seasonInfo != null) {
				try {
					serverTick = seasonInfo.getLong("server_tick");
					created = seasonInfo.getLong("created");
					int tickOfDay = (int) ((long) getTick() % (HOUR_TICKS * DAY_HOURS));

					int ig_h = (tickOfDay / HOUR_TICKS);
					int ig_m = (tickOfDay % HOUR_TICKS / 30);

					int ticksInSeason = (int) ((long) (getTick() - SEASON_OFFSET_TICKS) % JY_TICKS);
					int season = (int) (ticksInSeason / SEASON_TICKS);

					String ig_time;

					ig_time = String.format(Locale.ROOT, "%02d:%02d", ig_h, ig_m);

					switch (season) {
					case 0:
						ig_time += " (Spring)";
						break;
					case 1:
						ig_time += " (Summer)";
						break;
					case 2:
						ig_time += " (Autumn)";
						break;
					case 3:
						ig_time += " (Winter)";
						break;
					default:
						break;
					}
					seasonInfo.put("ig_time", ig_time);

					double apiMinutesLeftInSeason = 6480 - (getDayOfSeason() * 72) - ((getTimeOfDay() * 3));
					/*
					 * Seems like the tick runs about five - eight minutes slow
					 * during a season. So we need to add five minutes. (Better
					 * to be early than late).
					 */
					apiMinutesLeftInSeason += (apiMinutesLeftInSeason / 6480) * 5;

					double apiNextSeasonChange = (double) System.currentTimeMillis() / (double) 1000
							+ (apiMinutesLeftInSeason * 60);

					long apiNextSeasonChangeLong = (long) (apiNextSeasonChange * 1000);
					seasonInfo.put("next_season_change_unixtime", apiNextSeasonChangeLong);

					String df = (String) DateFormat.format("EEEE dd MMMM yyyy kk:mm:ss zzzz", new Date(
							apiNextSeasonChangeLong));
					seasonInfo.put("next_season_change_string", df);

					int total_secsToSeasonChange = (int) (apiMinutesLeftInSeason * 60);
					seasonInfo.put("total_secs_to_season_change", total_secsToSeasonChange);

					int d = (total_secsToSeasonChange / 86400);
					int h = (total_secsToSeasonChange % 86400 / 3600);
					int m = (total_secsToSeasonChange % 3600 / 60);
					int s = (total_secsToSeasonChange % 3600 % 60);
					seasonInfo.put("next_season_change_countdown", String.valueOf(d) + " day" + (d != 1 ? "s" : "")
							+ "<br>" + String.valueOf(h) + " hour" + (h != 1 ? "s" : "") + "<br>" + String.valueOf(m)
							+ " minute" + (m != 1 ? "s" : "") + "<br>" + String.valueOf(s) + " second"
							+ (s != 1 ? "s" : ""));

				} catch (JSONException e) {
					e.printStackTrace();
					Log.d(TAG, "Error getting season info.");
				}
			} else {
				Log.d(TAG, "Error getting season info.");
			}
			return null;
		}

		protected long getTick() {
			long now = System.currentTimeMillis();
			long createdOffset = (now - new Date(created * 1000).getTime()) / 1000;
			long correctedTick = serverTick + (createdOffset * 10);
			return correctedTick;
		}

		protected int getDayOfSeason() {
			double timeInHours = (double) (getTick() - SEASON_OFFSET_TICKS) / HOUR_TICKS;
			double day = timeInHours / DAY_HOURS;
			return (int) day % SEASON_DAYS;
		}

		protected double getTimeOfDay() {
			double timeInHours = (double) getTick() / HOUR_TICKS;
			return Math.abs(timeInHours % DAY_HOURS);
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			TextView txtNextSeasonChange = (TextView) findViewById(R.id.txtNextSeasonChange);
			TextView txtCountdown = (TextView) findViewById(R.id.txtCountdown);
			TextView txtInGameTime = (TextView) findViewById(R.id.txtInGameTime);
			if (seasonInfo != null) {
				try {
					txtNextSeasonChange.setText(seasonInfo.getString("next_season_change_string"));
					txtCountdown.setText(Html.fromHtml(seasonInfo.getString("next_season_change_countdown")));
					txtInGameTime.setText(seasonInfo.getString("ig_time"));
				} catch (JSONException e) {
					e.printStackTrace();
					txtNextSeasonChange.setText("Error getting season info.");
					txtCountdown.setText("Error getting season info.");
				}
			}
		}

	}

}
