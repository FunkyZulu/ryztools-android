Welcome to Android Ryztools
===========================

Source of the Ryztools Android app.

Any help with further development is appreciated!


Code style
----------

General ADT setting, with the exceptions of:
* Indentation is one tab for all files.
* Line lengths are 120 characters (tab is four characters).


Setup
-----
1. Clone this repo:
    
```
git clone --recursive git@gitlab.com:rubdos/ryztools-android.git
```

2. Go to the tools directory and run the `php` script to generate the sqlite3 database from the sheets.

```
cd tools
php ryzomExtraToSQLite.php
```

3. Open Android Studio, File -> Open
4. Select the cloned project directory

License
-------
http://opensource.org/licenses/GPL-3.0 GPL 3.0
